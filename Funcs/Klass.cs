﻿namespace Funcs
{
    public static class Klass
    {
        public static int LD(string stroka1, string stroka2)
        {
            int l = stroka1.Length, ll = stroka2.Length;
            int [,] a = new int[l + 1, ll + 1];
            for (int i = 0; i <= l; i++) { a[i, 0] = i; }
            for (int i = 0; i <= ll; i++) { a[0, i] = i; }
            for (int i = 1; i <= l; i++) {
            for (int ii = 1; ii <= ll; ii++) {
            int f = a[i, ii - 1], e = 1 + a[i - 1, ii];
            int c = stroka1[i - 1], d = stroka2[ii - 1];
            int[] ar = new int[3];
            int g = a[i - 1, ii - 1], b;
            if (c == d) { b = 0; }
            else { b = 1; }
            ar[0] = e + 1;
            ar[1] = f + 1;
            ar[2] = g + b;
            b = int.MaxValue;
            foreach (int ee in ar) { if (ee < b) { b = ee; } }
            a[i, ii] = b; }}
            return a[l, ll];
        }
    }
}
